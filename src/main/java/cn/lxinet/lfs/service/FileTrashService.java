package cn.lxinet.lfs.service;

import cn.lxinet.lfs.entity.FileTrash;
import cn.lxinet.lfs.mapper.FileTrashMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 文件垃圾篓
 *
 * @author zcx
 * @date 2023/11/30
 */
@Service
public class FileTrashService extends ServiceImpl<FileTrashMapper, FileTrash> {
    private static final Logger LOGGER = LoggerFactory.getLogger(FileTrashService.class);
    @Autowired
    private FileTrashMapper fileTrashMapper;

}
