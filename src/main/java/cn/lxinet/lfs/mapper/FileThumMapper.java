package cn.lxinet.lfs.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.lxinet.lfs.entity.FileThum;
import org.apache.ibatis.annotations.Mapper;

/**
 * 文件缩略图
 *
 * @author zcx
 * @date 2023/11/25
 */
@Mapper
public interface FileThumMapper extends BaseMapper<FileThum> {

}
