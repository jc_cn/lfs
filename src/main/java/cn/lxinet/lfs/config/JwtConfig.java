package cn.lxinet.lfs.config;


import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import cn.lxinet.lfs.exception.BaseException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * jwt配置
 *
 * @author zcx
 * @date 2023/07/09
 */
@Component
public class JwtConfig {
    @Value("${config.app-id}")
    private String appId;
    @Value("${config.app-secret}")
    private String appSecret;
    @Value("${config.token-expire}")
    private long expire;
    private static String ISSUER = "";

    /**
     * 生成token
     * @return
     */
    public String genToken(String id, String secret){
        if (!appId.equals(id) || !appSecret.equals(secret)){
            throw new BaseException("appId或者appSecret不正确");
        }
        Algorithm algorithm = Algorithm.HMAC256(appSecret);
        JWTCreator.Builder builder = JWT.create().withIssuer(ISSUER).withIssuedAt(new Date()).
                withExpiresAt(new Date((Instant.now().getEpochSecond() + expire) * 1000));
        Map<String, String> claims = new HashMap<>();
        claims.put("appId", appId);
        claims.forEach((key,value)-> builder.withClaim(key, value));
        return builder.sign(algorithm);
    }

    /**
     * 解析jwt
     * @param token
     * @return
     */
    public boolean parseToken(String token) {
        try {
            if (StringUtils.isBlank(token)){
                return false;
            }
            Algorithm algorithm = Algorithm.HMAC256(appSecret);
            JWTVerifier verifier = JWT.require(algorithm).withIssuer(ISSUER).build();
            DecodedJWT jwt =  verifier.verify(token);
            Map<String, Claim> map = jwt.getClaims();
            Map<String, String> resultMap = new HashMap<>();
            map.forEach((k,v) -> resultMap.put(k, v.asString()));
            String thisAppId = resultMap.get("appId");
            return appId.equals(thisAppId);
        }catch (Exception e){
            return false;
        }
    }

}
